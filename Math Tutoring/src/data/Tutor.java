package data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Tutor {

    StringProperty name = new SimpleStringProperty();
    StringProperty aNumber = new SimpleStringProperty();

    Tutor(String name, String aNumber){
        this.name.setValue(name);
        this.aNumber.setValue(aNumber);
    }

    public String getaNumber() {
        return aNumber.get();
    }

    public String getName() {
        return name.get();
    }
}

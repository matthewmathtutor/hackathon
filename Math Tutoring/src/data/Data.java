package data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Data {
    private static Data singleInstance = null;

    ArrayList<Course> courseList = new ArrayList<>();
    ArrayList<Tutor> tutorList = new ArrayList<>();

    public static Data getInstance(){
        if (singleInstance == null){
            singleInstance = new Data();
        }
        return singleInstance;
    }

    private Data(){

        FileReader fileReader = null;
        try {
            fileReader = new FileReader("data/courses");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(fileReader);
        while(scanner.hasNext()){
            courseList.add(new Course(scanner.nextLine()));
        }


        try {
            fileReader = new FileReader("data/tutors");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        scanner = new Scanner(fileReader);
        while(scanner.hasNext()){
            String name = scanner.nextLine();
            String aNumber = scanner.nextLine();
            tutorList.add(new Tutor(name,aNumber));
        }

    }

    public static ArrayList<Course> getCourseList() {
        return singleInstance.courseList();
    }

    private ArrayList<Course> courseList(){
        return courseList;
    }

    public static Tutor getTutor(String aNumber){
        return singleInstance.tutor(aNumber);
    }

    private Tutor tutor(String aNumber){
        for (Tutor t:tutorList) {
            if (t.getaNumber().equals(aNumber)){
                return t;
            }
        }
        return null;
    }
}

package gui;

import data.Data;
import data.Tutor;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class QueuePageController {
    @FXML
    private TableView<ListItem> mathList;

    @FXML
    private TableView<ListItem> statsList;

    @FXML
    private Button addMath;

    @FXML
    private Button addStats;


    ObservableList<ListItem> mathQueue = FXCollections.observableList(new ArrayList<ListItem>());

    @FXML
    private TableColumn<ListItem, String> mathName;

    @FXML
    private TableColumn<ListItem, String> mathCourse;

    @FXML
    private TableColumn<ListItem, String> mathTime;

    @FXML
    private TableColumn<ListItem, String> mathDone;

    @FXML
    private TableColumn<ListItem, String> statsName;

    @FXML
    private TableColumn<ListItem, String> statsCourse;

    @FXML
    private TableColumn<ListItem, String> statsTime;


    public void initialize(){
        mathName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        mathCourse.setCellValueFactory(cellData -> cellData.getValue().courseProperty());
        mathTime.setCellValueFactory(cellData -> cellData.getValue().timeProperty());

        addMath.setOnMouseClicked(this::addMathClick);

        mathList.setRowFactory(tv -> {
            TableRow<ListItem> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (! row.isEmpty() && event.getButton()== MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    rowActivated(row.getIndex());
                }
            });
            return row;
        });
    }


    public void rowActivated(int index){

        ListItem listItem = mathQueue.get(index);

        PasswordDialog passwordDialog = new PasswordDialog();
        String pass = passwordDialog.getPassword();

        Tutor tutor = Data.getTutor(pass);
        if (tutor != null){
            mathQueue.remove(index);
        }
        
    }

    public void addMathClick(MouseEvent event){

        ListItem tempItem = new ListItem();


        mathQueue.addAll(tempItem);
        mathList.setItems(mathQueue);

    }


}

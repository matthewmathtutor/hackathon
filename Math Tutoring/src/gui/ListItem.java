package gui;

import data.Course;
import data.Data;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.util.ArrayList;
import java.util.Optional;


public class ListItem {

    private StringProperty name;
    private StringProperty course = new SimpleStringProperty("Initial");
    private StringProperty time = new SimpleStringProperty("Now!");


    ListItem(){
        TextInputDialog dialog = new TextInputDialog(null);
        dialog.setTitle("Join the Queue");

        dialog.setContentText("Name: ");
        Optional<String> res = dialog.showAndWait();
        if (!res.isPresent()){
            name.setValue(null);
            return;
        }
        this.name = new SimpleStringProperty(res.get());

        dialog.getEditor().clear();


        ArrayList<Course> list = Data.getCourseList();

        ChoiceDialog<Course> dialog2 = new ChoiceDialog<>(null, list);
        dialog.setTitle("");
        dialog.setHeaderText(null);
        dialog.setContentText("What course are you taking?");

        Optional<Course> result = dialog2.showAndWait();

        if(!result.isPresent()){
            name.setValue(null);
            return;
        }
        this.course = new SimpleStringProperty(result.get().toString());

    }


    ListItem(String name, String course, String time){
        this.name.setValue(name);
        this.course.setValue(course);
        this.time.setValue(time);
    }

    public String getName(){
        return name.getValue();
    }

    public String getCourse(){
        return course.getValue();
    }

    public String getTime(){
        return time.getValue();
    }



    public StringProperty nameProperty(){
        return name;
    }

    public StringProperty courseProperty() {
        return course;
    }

    public StringProperty timeProperty() {
        return time;
    }
}

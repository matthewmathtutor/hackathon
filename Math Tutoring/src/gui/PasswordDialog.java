package gui;

import data.Data;
import data.Tutor;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class PasswordDialog {

    PasswordField passwordField = new PasswordField();
    Stage stage = new Stage();
    String pass;

    PasswordDialog(){
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(passwordField);
        borderPane.setLeft(new Label("Password:  "));
        borderPane.setPadding(new Insets(15));
        stage.setScene(new Scene(borderPane));
    }

    public String getPassword(){

        passwordField.setOnAction((ActionEvent e) -> {
            pass = passwordField.getText();
            stage.close();
        });
        stage.showAndWait();
        return pass;
    }

}

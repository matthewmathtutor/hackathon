import data.Data;
import gui.QueuePageController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/QueuePage.fxml"));
        Parent viewPane = loader.load();
        QueuePageController queuePageController = (QueuePageController) loader.getController();

        Data data = Data.getInstance();

        Scene scene = new Scene(viewPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Math Tutoring Center");
        primaryStage.show();


    }

}
